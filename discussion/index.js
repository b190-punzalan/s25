console.log("hello world");

// Section - JSON Objects
/*
JSON stands for JavaScript Object Notation
- also used in other programming language, hence the "Notation" in its name
- JSON is not to be confised with Javascript objects since aside from the format itself, JS objects are exclusive to Javascript while JSON can be used by other languages as well
*/
// Syntax:
/*
{
	"propertyA": "valueA",
	"propertyB": "valueB"
}
*/

// mini-activity: create ng citis array of 3 JS objects with the following properties: city, province, country. 
// to create an array of objects: [{}, {}, {}]

/*let cities = [{
	city: "Tarlac City",
	province: "Tarlac",
	country: "Philippines"
}, 
{
	city: "Malolos City",
	province: "Bulacan",
	country: "Philippines"
},
{
	city: "Binan City",
	province: "Laguna",
	country: "Philippines"
}]*/
// console.log(cities);
// this is an example of JS object

// what if we try to make use of JSON for the above?

let cities = [{
	"city": "Tarlac City",
	"province": "Tarlac",
	"country": "Philippines"
}, 
{
	"city": "Malolos City",
	"province": "Bulacan",
	"country": "Philippines"
},
{
	"city": "Binan City",
	"province": "Laguna",
	"country": "Philippines"
}]
console.log(cities);
// so halos pareho lang pag na-log
// but since JSON is used by other languages as well, it is common to see it as JS object since it is the blueprint of the JSON Format

/*
What JSON does
- JSON is used for serializing different data types into bytes
- bytes is a unit of data that is composed of eight binary digits (1/0) that is used to represent a character(letters, numbers, typographical symbol
-serialization - process of converting data into a series of bytes for easier transmission/ transfer of info)
*/

// JSON Methods
	// JSON objectt cotain methods for parsing and converting data into/from JSON or stringified JSON

// Section - Stringify Method
/*
- stringified JSON is a JS object converted into a string (but in JSON format) to be used in other functions of a JS application
- they are commonly used in HTTP request where infomation is required to be sent and received in a stringified JSON format
- requests are an important part of programming where a frontend application communicates with a backend applic to perform diff tasks such as getting/creating data in a database
- a front applic is an appli used to interact with users to perform diff tasks and display info while the backend applic are commonly used for all business logic and data processing
- a database normally stores info/data that can be used in most applications
- commonly stored data in databases are user info, transaction records, and product info
-Node/Express JS are two of technologies that are used for creating backend applications which process requests from frontend application
- Node JS is a Java Runtime Environment (JRE)/software that is made to execute other software
- Express JS is a Node JS Framework that provides featuresfor easily creating web and mobile applications
*/

let batchesArr = [{batchName: "Batch X"}, {batchName: "Batch Y"}];
console.log(batchesArr);
console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

// mini activity: create a "data" JS object

// direct conversion of data type from JS object into stringified JSON
let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Batangas",
		country: "Philippines"
	}
});
console.log(data);
// an example where JSON is commonly used is on package.json files which an express JS application uses to keep track of the info regarding a repository/project 

// JSON.stringify with prompt()
/*
When info is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
the "property" name and the "value" name would have to be the same and this might be a little confusing, but this is to signidy that the description of the variable and the property name pertains to the same thing and that is the value of the variable itself
this also helps in code readability
this is commonly used when we try to send info to the backend application
Syntax:
	JSON.stringify({
	propertA: varaibleA,
	propertB: variableB
	})
-since we don't have a frontend application yet, we will use the prompt method in order to gather uder data
*/
let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city belong to?"),
};

let otherData = JSON.stringify({
	fname: fname,
	lname: lname,
	age: age,
	address: address
});

console.log(otherData);
console.log(JSON.parse(otherData));

// pwede bang idiretso JSON.stringify na?
// let fname = JSON.stringify(prompt("what is your first name?));
// pwede pa rin, kasi object pa rin naman ang nreturn pero nagkaron ng mga escape variables. so technically, di siya pwede

// Parse Method
/*
-converts the stringified JSON into Javascript Objects
- objects are common data types used in applications because of the complex data structures that can be 
*/

let batchesJSON = `[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]`;
console.log(batchesJSON);
console.log("Result of parse method:");
console.log(JSON.parse(batchesJSON));

// mini activity
console.log("Result of parse method:");
console.log(JSON.parse(data));
console.log(JSON.parse(otherData));

